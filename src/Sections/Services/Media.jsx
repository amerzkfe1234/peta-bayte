const Media = ({ title, icon }) => {
  return (
    <div className="media">
      {icon}
      <h3>{title}</h3>
      <p>Nulla dapibus neque augue, quis tincidunt tortor aliquam vitae.</p>
    </div>
  );
};

export default Media;
