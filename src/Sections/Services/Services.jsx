import React from "react";
import Media from "./../Services/Media";
import sprite from "../../Icons/sprite.svg";

const Services = () => {
  return (
    <div data-aos="fade-up" className="services container--bigger">
      <Media
        title="Fast Servers"
        icon={
          <span
            className="media__icon-container"
            style={{ background: "#f1c40f" }}
          >
            <svg className="media__icon">
              <use href={sprite + "#fast"}></use>
            </svg>
          </span>
        }
      />
      <Media
        title="Cloud Servers"
        icon={
          <span
            className="media__icon-container"
            style={{ background: "#0faff1" }}
          >
            <svg className="media__icon">
              <use href={sprite + "#skype"}></use>
            </svg>
          </span>
        }
      />
      <Media
        title="Domain Transfer"
        icon={
          <span
            className="media__icon-container"
            style={{ background: "#95a5a6" }}
          >
            <svg className="media__icon">
              <use href={sprite + "#move-arrows"}></use>
            </svg>
          </span>
        }
      />
      <Media
        title="Live Support"
        icon={
          <span
            className="media__icon-container"
            style={{ background: "#f1540f" }}
          >
            <svg className="media__icon">
              <use href={sprite + "#chat"}></use>
            </svg>
          </span>
        }
      />
    </div>
  );
};

export default Services;
