import React from "react";
import Appbar from "./../../Common/Appbar";
import Text from "./Text";
import Nav from "./Nav";
import Informations from "./Informations";

const Footer = () => {
  return (
    <footer className="footer">
      <Appbar />
      <div className="footer__bottom container container--bigger">
        <Text />
        <Nav />
        <Informations />
      </div>
    </footer>
  );
};

export default Footer;
