import React from "react";
import sprite from "../../Icons/sprite.svg";

const Inforations = () => {
  return (
    <div className="footer__informations">
      <p>
        <svg className="footer__icon">
          <use href={sprite + "#telephone"}></use>
        </svg>
        &nbsp; 0000_0000_0000
      </p>
      <p>
        <svg className="footer__icon">
          <use href={sprite + "#email"}></use>
        </svg>
        &nbsp; informationteam@ghosthost.com
      </p>
      <p>Lorem ipsum dolor sit, amet consectetur adipisicing elit.</p>
      <p>ipsum</p>
    </div>
  );
};

export default Inforations;
