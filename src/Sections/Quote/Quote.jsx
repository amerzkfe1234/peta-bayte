import React, { useRef, useState } from "react";
import quoteImage from "../../images/quote.jpeg";

const Quote = () => {
  const [dotes, setDotes] = useState([
    {
      number: 1,
      selected: true,
    },
    {
      number: 2,
      selected: false,
    },
    {
      number: 3,
      selected: false,
    },
  ]);
  const slider = useRef();
  const quotes = {
    quote1: useRef(),
    quote2: useRef(),
    quote3: useRef(),
  };

  const onNav = (quote, number) => {
    slider.current.scrollLeft = quote.current.offsetLeft;
    setDotes((oldDotes) => {
      const newDotes = [...oldDotes];
      newDotes.map((dote) => {
        if (dote.number === number) {
          dote.selected = true;
          return dote;
        }
        dote.selected = false;
        return dote;
      });
      return newDotes;
    });
  };

  return (
    <div className="quote">
      <img src={quoteImage} className="quote__image" alt="" />
      <div
        ref={slider}
        className="quote__content container container--absolute"
      >
        <div data-aos="zoom-in" className="quote__slider">
          {[1, 2, 3].map((quoteNumber) => (
            <div
              key={quoteNumber}
              ref={quotes[`quote${quoteNumber}`]}
              className="quote__slide container--bigger"
              id={`quote${quoteNumber}`}
            >
              <p>
                “Suspendisse tempor turpis odio, sit amet cursus leo consequat
                non. Maecenas lacinia faucibus enimqui interdum dolor pulvinar
                vitae.”
              </p>
              <p className="quote__author">John Doe</p>
            </div>
          ))}
        </div>
      </div>
      <div className="quote__nav container container--bigger container--bigger-absolute">
        {dotes.map((dote) => (
          <button
            key={dote.number}
            onClick={() => onNav(quotes[`quote${dote.number}`], dote.number)}
            style={{
              background: dote.selected ? "var(--color-grey-darker)" : "#fff",
            }}
          ></button>
        ))}
      </div>
    </div>
  );
};

export default Quote;
