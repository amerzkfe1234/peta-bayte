import React from "react";

import Appbar from "../../Common/Appbar";
import Intro from "./Intro";
import Join from "./Join";

const Header = () => {
  return (
    <header className="header">
      <Appbar variant="header" />
      <Intro />
      <Join />
    </header>
  );
};

export default Header;
