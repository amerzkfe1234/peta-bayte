import React from "react";
import serversWebpImg from "../../images/servers.webp";
import serversJpgImg from "../../images/servers.jpg";

const Intro = () => {
  return (
    <div className="intro">
      <picture>
        <source type="image/webp" srcSet={serversWebpImg} />
        <img className="intro__image" src={serversJpgImg} alt="" />
      </picture>
      <div data-aos="fade-in" data-aos-delay="300" className="intro__text">
        <h1>HELLO!</h1>
        <h2>We love our work...</h2>
        <h2>And We're taking it seriously...</h2>
      </div>
    </div>
  );
};

export default Intro;
