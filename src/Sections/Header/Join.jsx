import React from "react";

const Join = () => {
  return (
    <div className="join">
      <div className="join__parallelogram join__login">
        <p className="join__text">LOGIN</p>
      </div>
      <div className="join__parallelogram join__register">
        <p className="join__text">REGISTER</p>
      </div>
    </div>
  );
};

export default Join;
