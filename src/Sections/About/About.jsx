import React from "react";
import Text from "./Text";
import Team from "./Team";

const About = () => {
  return (
    <section className="about container--bigger">
      <Text />
      <Team />
    </section>
  );
};

export default About;
