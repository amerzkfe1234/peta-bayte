import React from "react";

const Text = () => {
  return (
    <div data-aos="fade-down-right" className="about__text">
      <h2>About Us</h2>
      <p>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent
        feugiat eget diam sit amet molestie.Curabitur. ut ipsum bibendum tortor
        lacinia fermentum nec joeıa.Mauris at augue libero. Fusce posuere,
        sapien nec poır.Uaee augsuada sed mauris in tincidunt...
      </p>
    </div>
  );
};

export default Text;
