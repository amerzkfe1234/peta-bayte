import React from "react";
import sprite from "../../Icons/sprite.svg";

const Media = ({ name, rule, Image }) => {
  return (
    <div className="about__media">
      {Image}
      <h3 className="media-name">{name}</h3>
      <p className="media-rule">{rule}</p>
      <div className="media-links">
        <svg className="media-link">
          <use href={sprite + "#facebook"}></use>
        </svg>
        <svg className="media-link">
          <use href={sprite + "#twitter"}></use>
        </svg>
        <svg className="media-link">
          <use href={sprite + "#link"}></use>
        </svg>
      </div>
    </div>
  );
};

export default Media;
