import React from "react";
import designer1 from "../../images/designer1.jpg";
import designer2 from "../../images/designer2.jpg";
import designer3 from "../../images/designer3.jpg";

import Media from "./Media";

const Team = () => {
  return (
    <div data-aos="fade-up-left" className="about__team">
      <Media
        Image={
          <img className="media-img" src={designer1} alt={`team member ${1}`} />
        }
        name="Jonny Doe"
        rule="Designer"
      />
      <Media
        Image={
          <img className="media-img" src={designer2} alt={`team member ${2}`} />
        }
        name="Jonny Doe"
        rule="Designer"
      />
      <Media
        Image={
          <img className="media-img" src={designer3} alt={`team member ${3}`} />
        }
        name="Jonny Doe"
        rule="Designer"
      />
    </div>
  );
};

export default Team;
