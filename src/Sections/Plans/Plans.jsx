import React from "react";
import plansImage from "../../images/plans.jpeg";
import Card from "./Card";

const Plans = () => {
  const specifications = [
    {
      title: "Disk Space (GB)",
      number: "50",
    },
    {
      title: "Subdomains",
      number: "50",
    },
    {
      title: "Transfer (GB)",
      number: "50",
    },
    {
      title: "Data bases",
      number: "1",
    },
    {
      title: "Dashboards",
      number: "",
    },
    {
      title: "Control Panel & FTP",
      number: "",
    },
    {
      title: "Free Support",
      number: "",
    },
  ];

  return (
    <section className="plans">
      <img alt="" className="plans__image" src={plansImage}></img>
      <div
        data-aos="fade-in"
        className="plans__content container--bigger container--bigger-absolute"
      >
        <Card
          percentage="--25"
          packageSize="basic package"
          specifications={specifications}
          price="20"
          color="#fbc934"
        />
        <Card
          percentage="--50"
          packageSize="normal package"
          specifications={specifications}
          price="20"
          color="#7f8c8d"
        />
        <Card
          percentage="--75"
          packageSize="big package"
          specifications={specifications}
          price="20"
          color="#e65a3d"
        />
        <Card
          percentage="--100"
          packageSize="biggest package"
          specifications={specifications}
          price="20"
          color="#5dcfb9"
        />
      </div>
    </section>
  );
};

export default Plans;
