import React from "react";

import sprite from "../../Icons/sprite.svg";

const Card = ({ packageSize, percentage, specifications, price, color }) => {
  return (
    <div style={{ borderTop: `5px solid ${color}` }} className="card">
      <svg
        style={{ color, border: `5px solid ${color}` }}
        className={`card__star ${percentage}`}
      >
        <use href={sprite + "#star"}></use>
      </svg>
      <p className="card__backage-size">{packageSize}</p>
      <div className="card__divider"></div>
      <div className="card__specifications">
        {specifications.map((specification) => (
          <div key={specification.title} className="card__specifications-item">
            <p>{specification.title}</p>
            <p style={{ color }}>{specification.number}</p>
          </div>
        ))}
      </div>
      <div className="card__divider"></div>
      <div
        style={{ borderBottom: `5px solid ${color}` }}
        className="card__price"
      >
        <span className="card__dolor">$</span>{" "}
        <span className="card__price-number">{price}</span>
        <span className="card__month"> /Mo</span>
      </div>
      <div className="card__action">Select Plan</div>
    </div>
  );
};

export default Card;
