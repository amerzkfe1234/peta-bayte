import React from "react";
import sprite from "../../Icons/sprite.svg";

const Field = () => {
  return (
    <div data-aos="fade-left" data-aos-delay="300" className="domain__field">
      <span className="domain__www">www.</span>
      <input className="domain__input" />
      <span className="domain__extension">
        .com
        <svg className="domain__arrow">
          <use href={sprite + "#arrow-down"}></use>
        </svg>
      </span>
    </div>
  );
};

export default Field;
