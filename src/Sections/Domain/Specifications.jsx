import React from "react";

const Specifications = () => {
  return (
    <div
      data-aos="fade-right"
      data-aos-delay="300"
      className="domain__specifications"
    >
      <div className="domain__row ">
        <span className="domain__ceil domain__first-row-ceil domain__first-col-ceil">
          .com
        </span>
        <span className="domain__ceil domain__first-row-ceil">1 yer</span>
        <span className="domain__ceil domain__first-row-ceil">$ 9.00 USD</span>
        <span className="domain__ceil domain__first-row-ceil">
          Transfer Price no
        </span>
      </div>
      <div className="domain__row">
        <span className="domain__ceil domain__first-col-ceil">.net</span>
        <span className="domain__ceil">1 yer</span>
        <span className="domain__ceil">$ 9.00 USD</span>
        <span className="domain__ceil">Transfer Price no</span>
      </div>
    </div>
  );
};

export default Specifications;
