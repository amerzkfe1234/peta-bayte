import React from "react";

import Field from "./Field";
import Specifications from "./Specifications";
import Text from "./Text";

const Domain = () => {
  return (
    <section className="domain container">
      <Text />
      <Field />
      <Specifications />
    </section>
  );
};

export default Domain;
