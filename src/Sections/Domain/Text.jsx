import React from "react";

const Text = () => {
  return (
    <div data-aos="fade-in" className="domain__text">
      <h3>Would you like to buy a new domain ?</h3>
      <p>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse
        auctor mauris ac nulla interdum, id molestier.
      </p>
    </div>
  );
};

export default Text;
