import "./App.css";
import "./styles/appbar.css";
import "./styles/header.css";
import "./styles/domain.css";
import "./styles/plans.css";
import "./styles/quote.css";
import "./styles/about.css";
import "./styles/services.css";
import "./styles/footer.css";

import Header from "./Sections/Header/Header";
import Domain from "./Sections/Domain/Domain";
import Plans from "./Sections/Plans/Plans";
import About from "./Sections/About/About";
import Quote from "./Sections/Quote/Quote";
import Services from "./Sections/Services/Services";
import Footer from "./Sections/Footer/Footer";

function App() {
  return (
    <div>
      <Header />
      <Domain />
      <Plans />
      <About />
      <Quote />
      <Services />
      <Footer />
    </div>
  );
}

export default App;
