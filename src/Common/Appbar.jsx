import React from "react";

const Appbar = ({ variant }) => {
  const containerClasses = `appbar container--bigger  ${
    variant === "header" ? "appbar--header container--bigger-absolute" : ""
  }`;

  return (
    <div className={containerClasses}>
      <title className="appbar__title">
        <button data-text="Ghost&nbsp;Host">Ghost Host</button>
      </title>
      <ul className="appbar__links-container">
        <li className="appbar__link-container">
          <button className="appbar__link appbar__link-active ">Home</button>
        </li>
        <li className="appbar__link-container">
          <button className="appbar__link">Domain</button>
        </li>
        <li className="appbar__link-container">
          <button className="appbar__link">Hosting</button>
        </li>
        <li className="appbar__link-container">
          <button className="appbar__link">Clintes</button>
        </li>
        <li className="appbar__link-container">
          <button className="appbar__link">Blog</button>
        </li>
        <li className="appbar__link-container">
          <button className="appbar__link">Support</button>
        </li>
      </ul>
    </div>
  );
};

export default Appbar;
